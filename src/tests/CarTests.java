package tests;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import automobiles.Car;

class CarTests 
{	
	@Test
	void testInitialLocation()
	{
		Car vaz = new Car(20);
		assertEquals(vaz.getLocation(), 50);
	}
	
	@Test
	void testGetSpeed()
	{
		Car vaz = new Car(10);
		assertEquals(vaz.getSpeed(), 10);
	}
	
	@Test
	void testGetLocation()
	{
		Car vaz = new Car(10);
		assertEquals(vaz.getLocation(), 50);
	}
	
	@Test
	void testMoveRight()
	{
		Car vaz = new Car(10);
		vaz.moveRight();
		assertEquals(vaz.getLocation(), 60);
	}
	
	@Test
	void testMoveLeft()
	{
		Car vaz = new Car(20);
		vaz.moveLeft();
		assertEquals(vaz.getLocation(), 30);
	}
	
	@Test
	void testMoveRightMaxPos()
	{
		Car vaz = new Car(51);
		vaz.moveRight();
		assertEquals(vaz.getLocation(), 100);
	}
	
	@Test
	void testMoveLeftMaxPos()
	{
		Car vaz = new Car(51);
		vaz.moveLeft();
		assertEquals(vaz.getLocation(), 0);
	}
	
	@Test
	void testAccelerate()
	{
		Car vaz = new Car(20);
		vaz.accelerate();
		assertEquals(vaz.getSpeed(), 21);
	}
	
	@Test
	void testStop()
	{
		Car vaz = new Car(20);
		vaz.stop();
		assertEquals(vaz.getSpeed(), 0);
	}
	
	@Test
	void testCatchException()
	{
		try
		{
			Car vaz = new Car(-1);
			fail("IllegalArgumentException should be thrown - negative speed.");
		}
		
		catch(IllegalArgumentException e)
		{
			//success
		}
		
		catch(Exception e)
		{
			fail("Wrong exception type!");
		}
	}
}
