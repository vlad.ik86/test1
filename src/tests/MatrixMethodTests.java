package tests;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import utilities.MatrixMethod;

class MatrixMethodTests 
{
	@Test
    void testCheckArraySizes()
    {
        int[][] array = {{1, 2, 3, 4}, {4, 5, 6, 7}};
        int[][] dupe = MatrixMethod.duplicate(array);

        assertEquals(array.length, 2);
        assertEquals(array[0].length, 4);
        assertEquals(array[1].length, 4);
        
        assertEquals(dupe.length, 2);
        assertEquals(dupe[0].length, 8);
        assertEquals(array[1].length, 4);
    }
	
	@Test
	void testArrayIndexes()
	{
		int[][] array = {{1, 2, 3, 4}, {4, 5, 6, 7}};
        int[][] dupe = MatrixMethod.duplicate(array); 
        			 // {{1, 1, 2, 2, 3, 3, 4, 4,}, {4, 4, 5, 5, 6, 6, 7, 7}}
        
        assertEquals(array[0][0], 1);
        assertEquals(array[0][1], 2);
        assertEquals(array[1][2], 6);
        assertEquals(array[1][3], 7);
        
        assertEquals(dupe[0][0], 1);
        assertEquals(dupe[0][1], 1);
        assertEquals(dupe[1][6], 7);
        assertEquals(dupe[1][7], 7);
	}
	
	@Test
	void testDupeSuccess()
	{
		int[][] array = {{1, 2, 3, 4}, {4, 5, 6, 7}};
        int[][] realDupe = MatrixMethod.duplicate(array);
        int[][] hardDupe = {{1, 1, 2, 2, 3, 3, 4, 4,}, {4, 4, 5, 5, 6, 6, 7, 7}};
        
        assertEquals(realDupe[0][0], hardDupe[0][0]);
        assertEquals(realDupe[0][1], hardDupe[0][1]);
        assertEquals(realDupe[0][2], hardDupe[0][2]);
        assertEquals(realDupe[0][3], hardDupe[0][3]);
        
        assertEquals(realDupe[1][4], hardDupe[1][4]);
        assertEquals(realDupe[1][5], hardDupe[1][5]);
        assertEquals(realDupe[1][6], hardDupe[1][6]);
        assertEquals(realDupe[1][7], hardDupe[1][7]);
        
	}
	
}